# Information / Информация

Скрипт для получения внешнего IP и обновления IP в зоне CloudFlare.

## Права для скрипта

Для работы скрипта необходимы следующие права:

- read
- test

## Получение "cfDNSID":

Чтобы получить значение для переменной **cfDNSID**, необходимо выполнить следующую команду:

```
curl -X GET "https://api.cloudflare.com/client/v4/zones/API_ZONE_ID/dns_records"  \
-H "X-Auth-Email: USER_EMAIL"                                                     \
-H "X-Auth-Key: USER_TOKEN"                                                       \
-H "Content-Type: application/json" | python -mjson.tool
```

## Support / Поддержка

- [Forum](https://sysadmins.community/)
- [Documentation](https://sysadmins.wiki/)

## Donation / Пожертвование

- [Liberapay](https://liberapay.com/marketplace/donate)
- [Patreon](https://patreon.com/marketplace)
